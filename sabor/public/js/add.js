
firebase.initializeApp({
    apiKey: 'AIzaSyDM-OqCjLcSgTJjULJMQzmyidZeoe8hNDE',
    authDomain: 'sabor-f4fa3.firebaseapp.com',
    projectId: 'sabor-f4fa3'
  });





  
  // Initialize Cloud Firestore through Firebase
  var db = firebase.firestore();

  function guardarcte(){
      var  correo = document.getElementById('email-input').value;
      var  nombre = document.getElementById('nombre-input').value;
      var  contacto = document.getElementById('contacto-input').value;
      var  acumulado = document.getElementById('acumulado-input').value;
    db.collection("clientes").add({
        correo: correo,
        nombre: nombre,
        contacto: contacto,
        acumulado: acumulado
    })
    .then(function(docRef) {
        console.log("Cliente Agregado Correctamente con ID: ", docRef.id);
        document.getElementById('email-input').value = '';
        document.getElementById('nombre-input').value = '';
        document.getElementById('contacto-input').value = '';
        acumulado = document.getElementById('acumulado-input').value = 0;

    })
    .catch(function(error) {
        console.error("Error al Agregar al Cliente: ", error);
    });
  }


  //Leer Datos de Firebase
  var detdatos = document.getElementById('detdatos');
  db.collection("clientes").onSnapshot((querySnapshot) => {
    detdatos.innerHTML = '';
    querySnapshot.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data().nombre}`);
        detdatos.innerHTML += `
        <tr>
        <td>${doc.data().nombre}</td>
        <td>${doc.data().acumulado}</td>
        <td><button btn class="btn btn-small red accent-4" onclick="eliminar('${doc.id}')">Del</button></td>
        <td><button class="btn btn-small blue darken-1" onclick="editar('${doc.id}','${doc.data().correo}','${doc.data().nombre}','${doc.data().contacto}','${doc.data().acumulado}')">Edit</button></td>
      </tr>
        `
    });
});

//Borrar Datos de Fireboase
function eliminar(id){
    db.collection("clientes").doc(id).delete().then(function() {
        console.log("Document successfully deleted!");
        // window.location.href = 'index.html';
    }).catch(function(error) {
        console.error("Error removing document: ", error);
    });
}


//Actualizar Datos de Firebase


function editar(id, correo, nombre, contacto, acumulado){
    document.getElementById('email-input').value = correo;
    document.getElementById('nombre-input').value = nombre;
    document.getElementById('contacto-input').value = contacto;
    document.getElementById('acumulado-input').value = acumulado;

    var boton = document.getElementById('saveD');
    boton.innerHTML = 'Guardar Cambios';

    boton.onclick = function(){
        var washingtonRef = db.collection("clientes").doc(id);

        var correo = document.getElementById('email-input').value;
        var nombre = document.getElementById('nombre-input').value;
        var contacto = document.getElementById('contacto-input').value;
        var acumulado = document.getElementById('acumulado-input').value;

        return washingtonRef.update({
            correo: correo,
            nombre: nombre,
            contacto: contacto,
            acumulado: acumulado
        })
        .then(function() {
            console.log("Document successfully updated!");
            boton.innerHTML = 'Guardar';
            boton.onclick = guardarcte;
            document.getElementById('email-input').value = '';
            document.getElementById('nombre-input').value = '';
            document.getElementById('contacto-input').value = '';
            acumulado = document.getElementById('acumulado-input').value = 0;
        })
        .catch(function(error) {
            // The document probably doesn't exist.
            console.error("Error updating document: ", error);
        });
    }

}

function logout(){
    firebase.auth().signOut()
    .then(function(){
        console.log('Sesion Cerrada')
        window.location.href='index.html';
    })
    .catch(function(error){
        console.log(error)
    })
}


const btnToggle = document.querySelector('.toggle-btn');

btnToggle.addEventListener('click', function(){
    document.getElementById('sidebar').classList.toggle('active');
})

